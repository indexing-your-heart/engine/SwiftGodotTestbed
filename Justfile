build-driver:
    #!/bin/sh
    cd Driver
    swift build --configuration release
    if [ -d ".build/release/" ]; then
        cp .build/release/libDriver.dylib ../bin/osx
        cp -rf .build/release/SwiftGodotCore.framework ../bin/osx
    fi
    cd ..

clean:
    rm -rf Driver/.build