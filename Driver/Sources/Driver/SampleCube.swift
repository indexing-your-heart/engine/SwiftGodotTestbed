//
//  File.swift
//  
//
//  Created by Marquis Kurt on 10/4/23.
//

import Logging
import SwiftGodot

@NativeHandleDiscarding
final class SampleCube: Node3D {
    let logger = Logger(label: "godotengine.swiftgodot.driver")

    @Autovariant var flipRotation: Bool = false
    @Autovariant var rotateX: Bool = false
    @Autovariant var rotateZ: Bool = false

    required init() {
        SampleCube.initializeClass()
        super.init()
        logger.debug("\(SampleCube.self) initialized.")
    }

    override func _ready() {
        super._ready()
        let meshInstance = MeshInstance3D()
        meshInstance.mesh = makeMesh()
        self.addChild(node: meshInstance)
        logger.debug("Added a mesh instance: \(meshInstance)")    
    }

    override func _process(delta: Double) {
        rotateY(angle: flipRotation ? delta * -1 : delta)
        if rotateX { rotateX(angle: flipRotation ? delta * -1 : delta) }
        if rotateZ { rotateZ(angle: flipRotation ? delta * -1 : delta) }
    }

    private func makeMesh() -> BoxMesh {
        let mesh = BoxMesh()
        mesh.material = GD.load(path: "res://godot_box.tres") as? StandardMaterial3D
        if mesh.material == nil {
            logger.warning("Texture could not be assigned.")
        }
        logger.debug("Mesh created: \(mesh)")
        return mesh
    }
}

extension SampleCube: GodotInspectable {
    static var inspector: Inspector<SampleCube> {
        Inspector<SampleCube> {
            Toggle("flipped", property: #autoProperty(object: SampleCube.self, "flipRotation"))
            Group<SampleCube>("Additional Rotations", prefix: "sides") {
                Toggle("rotate_x", property: #autoProperty(object: SampleCube.self, "rotateX"))
                Toggle("rotate_z", property: #autoProperty(object: SampleCube.self, "rotateZ"))
            }
        }
    }
}

extension SampleCube {
    static func initializeClass() {
        let classInfo = ClassInfo<SampleCube>(name: StringName("\(SampleCube.self)"))
        classInfo.registerInspector()
    }
}
