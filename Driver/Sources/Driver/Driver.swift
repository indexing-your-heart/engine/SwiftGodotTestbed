// The Swift Programming Language
// https://docs.swift.org/swift-book

import Logging
import SwiftGodot

@GodotMain
class Driver: GodotExtensionDelegate {
    static var logger = Logger(label: "godotengine.swiftgodot.driver")

    func extensionWillInitialize() {
        LoggingSystem.bootstrap(GodotLogger.init)
    }

    func extensionDidInitialize(at level: GDExtension.InitializationLevel) {
        switch level {
        case .scene:
            registerNode(SampleCube.self)
        default:
            break
        }
    }

    func extensionWillDeinitialize(at level: GDExtension.InitializationLevel) {
        Driver.logger.debug("Deinit at level: \(level)")
    }

    deinit {
        print("Goodbye, y'all.")
    }

    func registerNode<T: Wrapped>(_ node: T.Type) {
        register(type: T.self)
        Driver.logger.debug("Registered type: \(node.self)")
    }
}
