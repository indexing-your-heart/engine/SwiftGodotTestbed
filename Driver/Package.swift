// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Driver",
    platforms: [.macOS(.v13), .iOS(.v16)],
    products: [
        .library(
            name: "Driver",
            type: .dynamic,
            targets: ["Driver"]),
    ],
    dependencies: [
//        .package(name: "SwiftGodot", path: "../../Packages/SwiftGodot"),
        .package(url: "https://github.com/apple/swift-log.git", from: "1.0.0"),
        .package(url: "https://gitlab.com/Indexing-Your-Heart/engine/SwiftGodot",
                 branch: "root")
    ],
    targets: [
        .target(
            name: "Driver",
            dependencies: ["SwiftGodot", .product(name: "Logging", package: "swift-log")],
            linkerSettings: [
                .unsafeFlags(["-Xlinker", "-undefined","-Xlinker", "dynamic_lookup"])
            ]),
        .testTarget(
            name: "DriverTests",
            dependencies: ["Driver"]),
    ]
)
