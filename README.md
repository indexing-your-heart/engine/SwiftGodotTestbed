# Swift Godot Testbed

This repository serves as a testbed for the SwiftGodot framework,
including SwiftGodotCore, SwiftGodotMacros, and SwiftGodotLogging.

## Getting started

**Required Tools**  
- An Apple Silicon Mac running macOS Ventura or later
- Xcode 15 or later, or Swift 5.9 or later
- Just (https://just.systems)

Clone the repository, then run `just build-driver` to build the
Swift package and copy the files into the game. From here, you can
run Godot normally.